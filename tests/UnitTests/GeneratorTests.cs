using System;
using Xunit;
using System.Linq;

using FizzBuzz;
using Moq;

namespace UnitTests;

public class GeneratorTests
{
    private readonly Generator _generator = new(new Encoder());

    [Fact]
    public void GivenNegative_ThenThrows()
    {
        Assert.Throws<ArgumentOutOfRangeException>(() => _generator.Generate(-1));
    }

    [Fact]
    public void Given0_ThenReturnsEmptyEnumerable()
    {
        var items = _generator.Generate(0);

        Assert.Empty(items);
    }

    [Fact]
    public void Given1_ThenReturns1Item()
    {
        var items = _generator.Generate(1);

        Assert.Single(items);
    }

    [Fact]
    public void Given1_ThenReturnsEncodedItem()
    {
        var items = _generator.Generate(1);

        Assert.Collection(items,
            item => Assert.Equal("1", item));
    }

    // while the intent of this test is the same as the previous,
    // it's probably a better test, given that it ensures that the
    // encoder was called with the proper input,
    // Though it seems to be only marginally better.

    [Fact]
    public void Given1_ThenReturnsEncodedItemMocked()
    {
        var encoder = new Mock<IEncoder>();
        var generator = new Generator(encoder.Object);

        var items = generator.Generate(1);

        encoder.Verify(e => e.Encode(1), Times.Once);
    }

    // The following tests were green from the beginning
    // so didn't require new production code to make them pass.
    // Therefore they don't adhere to the strict TDD rules.
    // I wrote them mainly to try out the xunit features,
    // but they still give me more confidence.
    // Are they just bloat?

    [Fact]
    public void Given6_ThenReturns6EncodedItems()
    {
        var items = _generator.Generate(6);
        
        Assert.Collection(items,
            item => Assert.Equal("1", item),
            item => Assert.Equal("2", item),
            item => Assert.Equal("Fizz", item),
            item => Assert.Equal("4", item),
            item => Assert.Equal("Buzz", item),
            item => Assert.Equal("Fizz", item));
    }

    // This test forced me to extract an interface
    // for the Encoder so that I could mock it properly.
    // Specifically, I needed an interface (or base class)
    // to be able to use It.IsAny

    [Fact]
    public void Given100_ThenCallsEncoder100TimesWithVerify()
    {
        var encoder = new Mock<IEncoder>();
        var generator = new Generator(encoder.Object);

        var items = generator.Generate(100);

        encoder.Verify(e => e.Encode(It.IsAny<int>()), Times.Exactly(100));
    }
}
