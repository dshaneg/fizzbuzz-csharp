using Xunit;

namespace UnitTests;

public class EncoderTests
{
    [Theory]
    [InlineData(1, "1")]
    [InlineData(2, "2")]
    [InlineData(3, "Fizz")]
    [InlineData(6, "Fizz")]
    [InlineData(5, "Buzz")]
    [InlineData(10, "Buzz")]
    [InlineData(15, "FizzBuzz")]
    [InlineData(30, "FizzBuzz")]
    [InlineData(100, "Buzz")]
    [InlineData(0, "FizzBuzz")]
    [InlineData(-3, "Fizz")]
    public void GivenNumber_ThenProperResponse(int testNumber, string expected)
    {
        var encoder = new FizzBuzz.Encoder();

        Assert.Equal(expected, encoder.Encode(testNumber));
    }
}
