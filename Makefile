test:
	dotnet test tests/UnitTests --nologo --no-restore

test-verbose:
	dotnet test tests/UnitTests --nologo --no-restore --logger "console;verbosity=detailed"

run:
	dotnet run --project src/FizzBuzz
