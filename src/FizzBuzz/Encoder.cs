namespace FizzBuzz
{
    public interface IEncoder
    {
        string Encode(int number);
    }

    public class Encoder : IEncoder
    {
        public string Encode(int number)
        {
            string response = "";

            if (IsMultiple(number, 3))
                response = "Fizz";

            if (IsMultiple(number, 5))
                response += "Buzz";

            if (response == "")
                response = number.ToString();

            return response;
        }

        private static bool IsMultiple(int number, int factor)
        {
            return number % factor == 0;
        }
    }
}
