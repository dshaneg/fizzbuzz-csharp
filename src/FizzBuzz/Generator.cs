namespace FizzBuzz
{
    public class Generator
    {
        private readonly IEncoder _encoder;

        public Generator(IEncoder encoder)
        {
            _encoder = encoder;
        }

        public IEnumerable<string> Generate(int countToGenerate)
        {
            if (countToGenerate < 0)
                throw new ArgumentOutOfRangeException(nameof(countToGenerate), "Can't be negative.");

            var list = new List<string>();

            for (int i = 1; i <= countToGenerate; i++)
                list.Add(_encoder.Encode(i));

            return list;
        }
    }
}
