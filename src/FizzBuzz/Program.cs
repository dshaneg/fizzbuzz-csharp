﻿using FizzBuzz;

var encoder = new Encoder();
var generator = new Generator(encoder);

var items = generator.Generate(100);

foreach(var item in items)
    Console.WriteLine(item);
